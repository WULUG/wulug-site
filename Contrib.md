# Teams
## Frontend
- Garrett (@LimeLava)
- Dennis (@EnnisHam) M
- Phong (@pvo1028)
- Aman (@dronzaya)
- Seb (@sebhoang01)

## Backend
- Amey (@ameh) M
- Aman (@dronzaya)
- Dennis (@EnnisHam)
- Phong (@pvo1028)
- Seb (@sebhoang01)


# Style Guide
## General 
### Indentation
4 Spaces

### Branch Names
YourName_WhatYouAreWorkingOn
E.g
ennis_models, garrett_contrib

### Variable naming
Use underscores and lower case for all variables(snake casing)

### Code line horizontal character limit
120 characters

### Comment formatting
```
/
 * Standard links:
 * {@link Foo} or {@linkplain Foo} or [[Foo]]
 *
 * Code links: (Puts Foo inside <code> tags)
 * {@linkcode Foo} or [[Foo]]
 */
export class Bar implements Foo {}

/ More details */
interface Foo {
    member: boolean;
}
```


## Typescript

### class and className
Use `class` for css stuff and `className` for typescript specific stuff


### Arrays
Leave a trailing comma after the last element

##### Bad
```typescript
Foo: int[] = [1, 2, 3, 4];
```
##### Good
```typescript
Foo: int[] = [1, 2, 3, 4,];
```

## Go Language

#### Bad
```js
{
    'var1': elem1,
    'var2': elem2
}
```
#### Good
```js
{
    'var1': elem1,
    'var2': elem2,
}
```


# Data Model
## Example

```
courseDataModel{
    'title'<string>: 'Intro to OOPs',
    'department'<string>: 'EECS',
    'level'<string>: '200',
    'instructor'<string>: 'Dr. Professor',
}
```


# Function mockup
## Example
```
function OOP() {
    return {
        'title'<string>: 'Intro to OOPs',
        'department'<string>: 'EECS',
        'level'<string>: '200',
        'instructor'<string>: 'Dr. Professor',
    }
}
```