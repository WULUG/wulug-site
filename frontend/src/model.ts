function IDD(): Object {
    return {
        'title': 'Introduction to Digital Design',
        'department': 'EECS',
        'level': 194,
        'instructor': 'Manira Sultana Rani',
        'time': 800,
        'length': 75,
        'days': ['M', 'W',],
    }
}
function IP(): Object {
    return {
        'title': 'Introduction to Programming',
        'department': 'EECS',
        'level': 211,
        'instructor': 'Huabo Lu',
        'time': 1000,
        'length': 75,
        'days': ['M', 'W',],
    }
}
function ASM(): Object {
    return {
        'title': 'Assembly Language Programming',
        'department': 'EECS',
        'level': 238,
        'instructor': 'Rajiv Bagai',
        'time': 930,
        'length': 75,
        'days': ['T', 'R',],
    }
}
function OOP(): Object {
    return {
        'title': 'Object Oriented Programming',
        'department': 'EECS',
        'level': 311,
        'instructor': 'Huabo Lu',
        'time': 1500,
        'length': 90,
        'days': ['M', 'W',"F"],
    }
}
function ICA(): Object {
    return {
        'title': 'Introduction to Computer Architecture',
        'department': 'EECS',
        'level': 394,
        'instructor': 'Hongsheng He',
        'time': 1605,
        'length': 75,
        'days': ['M', 'W',],
    }
}
function Data(): Object {
    return {
        'title': 'Data Structures',
        'department': 'EECS',
        'level': 400,
        'instructor': 'Huabo Lu',
        'time': 1735,
        'length': 85,
        'days': ['M', 'W',],
    }
}
function PP(): Object {
    return {
        'title': 'Programming Paradigms',
        'department': 'EECS',
        'level': 410,
        'instructor': 'Dukka B KC',
        'time': 1230,
        'length': 75,
        'days': ['M', 'W',],
    }
}
function CN(): Object {
    return {
        'title': 'Computer Networks',
        'department': 'EECS',
        'level': 464,
        'instructor': 'Remi Chou',
        'time': 1400,
        'length': 75,
        'days': ['T', 'R',],
    }
}
function ISE(): Object {
    return {
        'title': 'Intro to Software Engineering',
        'department': 'EECS',
        'level': 480,
        'instructor': 'Zhiyong Shan',
        'time': 800,
        'length': 75,
        'days': ['M', 'W',],
    }
}
function PLC(): Object {
    return {
        'title': 'Programming Language Concepts',
        'department': 'EECS',
        'level': 510,
        'instructor': 'Prakash V Ramanan',
        'time': 930,
        'length': 75,
        'days': ['T', 'R',],
    }
}
function OS(): Object {
    return {
        'title': 'Operating Systems',
        'department': 'EECS',
        'level': 540,
        'instructor': 'Vinod Vishnu Namboodiri',
        'time': 1230,
        'length': 75,
        'days': ['T', 'R',],
    }
}
function DAA(): Object {
    return {
        'title': 'Design and Analysis of Algorithms',
        'department': 'EECS',
        'level': 560,
        'instructor': 'Kaushik Sinha',
        'time': 930,
        'length': 75,
        'days': ['M', 'W',],
    }
}
function IDS(): Object {
    return {
        'title': 'Introduction to Database Systems',
        'department': 'EECS',
        'level': 665,
        'instructor': 'Keenan Doyle Jackson',
        'time': 1605,
        'length': 75,
        'days': ['M', 'W',],
    }
}